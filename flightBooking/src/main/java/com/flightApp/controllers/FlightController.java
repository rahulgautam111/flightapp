package com.flightApp.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flightApp.Entities.FlightBookingEntity;
import com.flightApp.Entities.FlightEntity;
import com.flightApp.Services.FlightService;

@RestController
@RequestMapping(name = "/api/v1.0/flight")
public class FlightController {

	@Autowired
	private FlightService flightService;

	@PostMapping("/booking")
	public ResponseEntity<FlightBookingEntity> bookFlight(@RequestBody FlightBookingEntity flight) {

		try {
			FlightBookingEntity flights = flightService.bookFlight(flight);

			return new ResponseEntity<FlightBookingEntity>(flights, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

//		}

	}

	@PostMapping("/")
	public ResponseEntity<String> addFlight(@RequestBody FlightEntity flightEntity) {

		try {

			FlightEntity resp = flightService.addNewFlight(flightEntity);
			if (resp != null) {
				return new ResponseEntity<String>("Flight add Successfully", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Error while updating ", HttpStatus.OK);
			}

		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	@PutMapping("/airlines/")
	public ResponseEntity<String> updateFlight(@RequestBody FlightEntity flight) {

		try {

			FlightEntity resp = flightService.updateFlight(flight);
			if (resp != null) {
				return new ResponseEntity<String>("Flight update  Successfully", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Error while updating Successfully", HttpStatus.OK);
			}

		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	@GetMapping("/airlines/search/")
	public ResponseEntity<List<FlightEntity>> searchFlights(@RequestHeader String source,
			@RequestHeader String destination) {

		try {
			List<FlightEntity> flights = flightService.searchFlights(source, destination);

			return new ResponseEntity<List<FlightEntity>>(flights, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception

			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}

	}

}
