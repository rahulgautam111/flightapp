package com.flightApp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.flightApp.Entities.FlightEntity;

@Repository
public interface FlightRepository extends JpaRepository<FlightEntity, Integer> {

	@Query("SELECT u FROM flights u WHERE u.status = 'running' and u.source = ?1 and u.destination = ?2")
	public List<FlightEntity> findByStatusAndSourceAndDestination(String source, String destination);
}
