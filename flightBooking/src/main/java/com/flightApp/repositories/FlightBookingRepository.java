package com.flightApp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flightApp.Entities.FlightBookingEntity;
import com.flightApp.Entities.FlightEntity;

@Repository
public interface FlightBookingRepository extends JpaRepository<FlightBookingEntity, Integer>{

}
