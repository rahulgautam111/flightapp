package com.flightApp.Entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.flightApp.cofig.MyGenerator;

@Entity(name = "bookedflights")
public class FlightBookingEntity {

	@Id
	@GeneratedValue(generator = MyGenerator.generatorName)
	private int id;

	private String flightNumber;

	private String date;

	private String source;

	private String destination;

	private String name;

	private String email;

	private int totalSeats;

	private String mealType;

	@ElementCollection
	private List<Passengers> passenger = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String from) {
		this.source = from;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTotalSeats() {
		return totalSeats;
	}

	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public List<Passengers> getPassenger() {
		return passenger;
	}

	public void setPassenger(List<Passengers> passenger) {
		this.passenger = passenger;
	}

}
