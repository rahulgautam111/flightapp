package com.flightApp.Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "flights")
public class FlightEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String flightNumber;

	private String source;

	private String destination;

	private String startDateTime;

	private String endDateTime;

	private String instrumentUsed;

	private String mealType;

	private String status;

	private String flightName;

	private long price;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String from) {
		this.source = from;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getInstrumentUsed() {
		return instrumentUsed;
	}

	public void setInstrumentUsed(String instrumentUsed) {
		this.instrumentUsed = instrumentUsed;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getFlightName() {
		return flightName;
	}

	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}

	@Override
	public String toString() {
		return "FlightEntity [id=" + id + ", flightNumber=" + flightNumber + ", from=" + source + ", to=" + destination
				+ ", startDateTime=" + startDateTime + ", endDateTime=" + endDateTime + ", instrumentUsed="
				+ instrumentUsed + ", mealType=" + mealType + "]";
	}

	public FlightEntity(int id, String flightNumber, String source, String destination, String startDateTime,
			String endDateTime, String instrumentUsed, String mealType) {
		super();
		this.id = id;
		this.flightNumber = flightNumber;
		this.source = source;
		this.destination = destination;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		this.instrumentUsed = instrumentUsed;
		this.mealType = mealType;
	}

	public FlightEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

}
