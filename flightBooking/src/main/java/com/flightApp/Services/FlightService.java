package com.flightApp.Services;

import java.util.List;

import com.flightApp.Entities.FlightBookingEntity;
import com.flightApp.Entities.FlightEntity;

public interface FlightService {

	public FlightBookingEntity bookFlight(FlightBookingEntity flightBookingEntity);

	public FlightEntity addNewFlight(FlightEntity flight);

	public FlightEntity updateFlight(FlightEntity flight);

	public List<FlightEntity> searchFlights(String source, String destination);

}
