package com.flightApp.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flightApp.Entities.FlightBookingEntity;
import com.flightApp.Entities.FlightEntity;
import com.flightApp.repositories.FlightBookingRepository;
import com.flightApp.repositories.FlightRepository;

@Service
public class FlightServiceImpl implements FlightService {

	@Autowired
	private FlightRepository flightRepository;
	
	private FlightBookingRepository flightBookingRepository;

	@Override
	public FlightEntity addNewFlight(FlightEntity flightEntity) {
		// TODO Auto-generated method stub
		FlightEntity resp = flightRepository.save(flightEntity);
		return resp;

	}

	@Override
	public FlightEntity updateFlight(FlightEntity flight) {
		// TODO Auto-generated method stub
		Optional<FlightEntity> flightResp = flightRepository.findById(flight.getId());

		if (flightResp.isPresent()) {
			FlightEntity updatedFlight = flightRepository.save(flight);
			return updatedFlight;
		} else {
			return null;
		}

	}

	@Override
	public List<FlightEntity> searchFlights(String source, String destination) {
		List<FlightEntity> searchedFlight = null;
		searchedFlight = flightRepository.findByStatusAndSourceAndDestination(source, destination);
		return searchedFlight;
	}

	@Override
	public FlightBookingEntity bookFlight(FlightBookingEntity flightBookingEntity) {
		// TODO Auto-generated method stub
		FlightBookingEntity bookedFlight = flightBookingRepository.save(flightBookingEntity);

		return bookedFlight;
	}

	public List getSearchedFligh() {
		return null;

	}

}
