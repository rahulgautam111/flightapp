package com.flightApp.Services;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.flightApp.DTOs.AuthDTO;
import com.flightApp.Entities.AuthEntity;

public interface AuthService {

	public AuthEntity validateUser(AuthDTO user);

}
