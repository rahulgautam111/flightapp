package com.flightApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.flightApp.Entities.FlightEntity;

@RestController
@RequestMapping(name = "/api/admin")
public class AdminController {

	@Autowired
	private RestTemplate restTemplate;

//	@PostMapping("/login/")
//	public ResponseEntity<?> loginUser(@RequestBody AuthDTO authDTO) {
//		try {
//
//			AuthEntity resp = authService.validateUser(authDTO);
//			if (resp != null) {
//				return new ResponseEntity<AuthEntity>(resp, HttpStatus.OK);
//			} else {
//				return new ResponseEntity<>("invalid credentials", HttpStatus.OK);
//			}
//
//		} catch (Exception e) {
//			// TODO: handle exception
//			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		}
//
//	}

	@PostMapping("/flight")
	public ResponseEntity<FlightEntity> addNewFlight(@RequestBody FlightEntity flight) {
		try {
			String url = "http://FLIGHTSERVICE/api/v1.0/flight/";
			FlightEntity resp = restTemplate.postForObject(url, flight, FlightEntity.class);
			return new ResponseEntity<FlightEntity>(resp, HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}
}
