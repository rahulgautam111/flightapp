package com.flightApp.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flightApp.Entities.AuthEntity;

@Repository
public interface AuthRepository extends JpaRepository<AuthEntity, Long> {
	Optional<AuthEntity> findByUserName(String userName);
}